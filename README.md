
```
#!R

# Link to the competition - https://www.kaggle.com/c/santander-product-recommendation/
# Use H2O / Domino to scale - https://blog.dominodatalab.com/using-r-h2o-and-domino-for-a-kaggle-competition/

rm(list = ls())
# setwd('C:/Users/urbonask/Documents/R/Santander kaggle')
# setwd('C:/Users/urbonask/Documents/R Explorations/20161115 Santander competition')
setwd('Y:/R/sant')
list.files()

library(data.table)
library(tidyverse)
library(lubridate)
library(dplyr)
library(pryr)
library(caret)
library(doParallel)

train <- fread('train_ver2.csv') %>% as.data.frame()
test <- fread('test_ver2.csv') %>% as.data.frame()
rm(test)
# testing if all customer_id are available in the training set that are in the test set
# trainID <- unique(train$ncodpers)
# testID <- unique(test$ncodpers)
# trainID <- as.data.frame(trainID)
# names(trainID) <- c('id','exists')
# testID <- as.data.frame(testID)
# names(testID) <- c('id')
# matching <- merge(testID,trainID, by = c('id'))
# table(matching$exists)
# rm(testID, trainID, matching)

# size of the dataset
# size <- object_size(train)
# as.numeric(size)/(1024^3)
# # 3.25 GB
# 
# sizeTest <- object_size(test)
# as.numeric(sizeTest)/(1024^3)

# view the Training dataset
head(train)
names(train)
str(train)
summary(train)

##########################
#### Cleaning the data
##########################

table(train$fecha_dato)
table(train$fecha_alta)

train$fecha_dato <- ymd(train$fecha_dato)
train$fecha_alta <- ymd(train$fecha_alta)

# remove NAs

# Number of missing values (to be dealt later)
sort(sapply(train, function(x) sum(is.na(x))),decreasing = TRUE)

train$ind_nuevo[is.na(train$ind_nuevo)] <- 1

train$fecha_alta[is.na(train$fecha_alta)] <- median(train$fecha_alta,na.rm=TRUE)

train$indrel[is.na(train$indrel)] <- 1

train$antiguedad[is.na(train$antiguedad)] <- min(train$antiguedad,na.rm=TRUE)
train$antiguedad[train$antiguedad<0] <- 0

train <- train %>% select(-tipodom,-cod_prov)

sum(is.na(train$ind_actividad_cliente))
train <- train[!is.na(train$ind_actividad_cliente), ]

train <- train[!is.na(train$ind_nomina_ult1), ]

new.incomes <- train %>%
  select(nomprov) %>%
  merge(train %>%
          group_by(nomprov) %>%
          summarise(med.income=median(renta,na.rm=TRUE)),by="nomprov") %>%
  select(nomprov,med.income) %>%
  arrange(nomprov)
train <- arrange(train,nomprov)
train$renta[is.na(train$renta)] <- new.incomes$med.income[is.na(train$renta)]
rm(new.incomes)

train$renta[is.na(train$renta)] <- median(train$renta,na.rm=TRUE)
train <- arrange(train,fecha_dato)

str(train)

train$indfall[train$indfall==""]                 <- "N"
train$tiprel_1mes[train$tiprel_1mes==""]         <- "A"
train$indrel_1mes[train$indrel_1mes==""]         <- "1"
train$indrel_1mes[train$indrel_1mes=="P"]        <- "5" # change to just numbers because it currently contains letters and numbers
train$indrel_1mes                             <- as.factor(as.integer(train$indrel_1mes))
train$pais_residencia[train$pais_residencia==""] <- "UNKNOWN"
train$sexo[train$sexo==""]                       <- "UNKNOWN"
train$ult_fec_cli_1t[train$ult_fec_cli_1t==""]   <- "UNKNOWN"
train$ind_empleado[train$ind_empleado==""]       <- "UNKNOWN"
train$indext[train$indext==""]                   <- "UNKNOWN"
train$indresi[train$indresi==""]                 <- "UNKNOWN"
train$conyuemp[train$conyuemp==""]               <- "UNKNOWN"
train$segmento[train$segmento==""]               <- "UNKNOWN"

########################
## Pepare for joining ##
########################

# HOW TO USE grep() TO SUBSET AND AVOID HARD-CODED INDECES
# temp <- behaveTrain[1:10,1:10]
# temp[,c(grep('ind_ahor_fin_ult1',names(temp)),grep('custid',names(temp)))]

features <- names(train)[grep('^ind_.+ult1$',names(train))]
features_t <- names(train)[c(grep('fecha_dato',names(train)),grep('ncodpers',names(train)))]
features_t <- c(features_t,features)
train_temp <- train[,features_t]
head(train_temp)

new_names = c()
for (x in names(train_temp[,features])) {
  new_names <- c(new_names,paste0(x,'_pre'))
}

new_names <- c(features_t[1:2],new_names)
setnames(train_temp,new_names)

train <- train[train$fecha_dato != ymd('2015-01-28'),]

train_temp$fecha_dato <- train_temp$fecha_dato + months(1)

# Joining
names(train)[grep('fecha_dato',names(train))] <- 'snapshot_date'
names(train)[grep('ncodpers',names(train))] <- 'custid'
names(train_temp)[grep('fecha_dato',names(train_temp))] <- 'snapshot_date'
names(train_temp)[grep('ncodpers',names(train_temp))] <- 'custid'

master <- left_join(train, train_temp, by = c('custid','snapshot_date'))

master$tenure_days <- as.numeric(master$snapshot_date - master$fecha_alta)
rm(train, train_temp)
head(master)

# create behavioral and demographic datasets
grep('snapshot_date',names(master)) #1
grep('segmento',names(master)) # 22
grep('tenure',names(master)) # 71 

# create index for behavioral data subset of "master"
behave_index <- c(grep('snapshot_date',names(master)),grep('custid',names(master)),grep('tenure',names(master)),(grep('segmento',names(master))+1):(grep('tenure',names(master))-1))

# demTrain <- master[,c(1,47,2:22,72)]
behaveTrain <- master[,behave_index]
rm(master,behave_index)

head(demTrain)
head(behaveTrain)
summary(behaveTrain)

##########################
#### Join datasets
##########################

# re-write join below to a less resource constrained solution with LAG
# calculate LAG - https://blog.exploratory.io/5-most-practically-useful-window-table-calculations-in-r-7e2c7ca431d9#.7y0mbzt5u

grep('ind_ahor_fin',names(behaveTrain))

#replace NAs in _pre period with 0 as all of the current month's additions will incremental
for (i in grep('ind_ahor_fin_ult1_pre',names(behaveTrain)):(grep('ind_recibo_ult1_pre',names(behaveTrain)))) {
  behaveTrain[is.na(behaveTrain[,i]),i] <- 0
}

# get indeces for first and last behavioral columns
start_ind <- grep('ind_ahor_fin_ult1',names(behaveTrain))[1]
end_ind <- grep('ind_recibo_ult1',names(behaveTrain))[1]
ind_cutoff <- length(names(behaveTrain[,start_ind:end_ind]))
last_ind <- ncol(behaveTrain)

# calculate difference between this month and previous one
for (i in 1:ind_cutoff) {
  behaveTrain <- cbind(behaveTrain,0)
  names(behaveTrain)[i+last_ind] <- paste0(names(behaveTrain)[i+3],'_DIFF')
  behaveTrain[,i+51] <- as.integer(behaveTrain[,i+3] - behaveTrain[,i+27])  
}

apply(behaveTrain[,4:75],2,sum) # check sum of columns to understand distribution
sort(sapply(behaveTrain, function(x) sum(is.na(x))),decreasing = TRUE) # check for NAs
summary(behaveTrain)
head(behaveTrain)

rm(end_ind, features, features_t, i, ind_cutoff, last_ind, new_names, start_ind, x)

##########################
#### create dataset with target variables
##########################

start_ind <- grep('ind_ahor_fin_ult1_DIFF',names(behaveTrain)) # 52
end_ind <- grep('ind_recibo_ult1_DIFF',names(behaveTrain)) # 75

target_vars <- behaveTrain[,c(1:3,start_ind:end_ind)]
head(target_vars)
summary(target_vars)

rm(start_ind, end_ind)

# change -1 to 0 as the competition only evaluates precission of addition, not dropping
start_ind <- grep('ind_ahor_fin_ult1_DIFF',names(target_vars)) 
end_ind <- grep('ind_recibo_ult1_DIFF',names(target_vars)) 

for (i in start_ind:end_ind) {
  target_vars[target_vars[,i] == -1, i] <- 0
}

# change the name to original target name
for (i in start_ind:end_ind) {
  names(target_vars)[i] <- substr(names(target_vars)[i],1,nchar(names(target_vars)[i])-5)
}

head(target_vars)
apply(target_vars[,start_ind:end_ind],2,sum)

# distribtuion of monthly additions
table(apply(target_vars[,start_ind:end_ind],1,sum))
#        0        1        2        3        4        5        6        7 
# 12454077   450603    73232    20522     2321      149        9        1 

# Add merge_date to join to the features
names(target_vars)[1] <- 'prediction_date'
target_vars$snapshot_date <- target_vars$prediction_date - months(1)
new_index <- c(grep('snapshot_date',names(target_vars)),
               grep('prediction_date',names(target_vars)),
               grep('custid',names(target_vars)),
               grep('tenure_days',names(target_vars)),
               grep('ind_ahor_fin_ult1',names(target_vars)) : grep('ind_recibo_ult1',names(target_vars))
              )
target_vars <- target_vars[,c(new_index)]
head(target_vars)

##########################
#### Feature engineering
##########################

# http://stackoverflow.com/questions/30337250/conditional-cumulative-sum-using-dplyr
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! https://github.com/hadley/multidplyr/blob/master/vignettes/multidplyr.md !!!!!!!!!!!!!!!!!!!!!!!!!!!

# join to itself to
# 1) get previous month's figure and calculate difference (+1 / -1, 0) --> transform into a) target variable 1/0 and b) additional features showing direction of behavior
# 2) get sum of months product was owned
# 3) get stability of subscription (negative sums, positive sums, variance in the monthly differences --> divide by total months since first purchase / total months of tracking)
# 4) sum of total products owned and its variance over time

# Calculate totals of each month:
  
  # total products owned on a given month
start_ind <- grep('ind_ahor_fin_ult1',names(behaveTrain))[1]
end_ind <- grep('ind_recibo_ult1',names(behaveTrain))[1]

behaveTrain$total_owned_m <- apply(behaveTrain[, start_ind : end_ind], 1, sum)
    
    # distribution of monthly ownership status
    # table(behaveTrain$total_owned_m)
    #       0       1       2       3       4       5       6       7       8       9      10      11      12      13      14      15 
    # 2534488 6766847 1820787  727045  425177  273643  198896  133250   71687   31759   11961    4077    1049     215      24       9

    #monthly distribution of monthly owned products
    # table(behaveTrain$snapshot_date, behaveTrain$total_owned_m)
    
  # is owner on a given month
behaveTrain$is_owner <- apply(behaveTrain[, start_ind : end_ind], 1, max)
  
    # table(behaveTrain$is_owner)

  # total added

start_diff_ind <- grep('ind_ahor_fin_ult1_DIFF',names(behaveTrain))[1]
end_diff_ind <- grep('ind_recibo_ult1_DIFF',names(behaveTrain))[1]

behaveTrain$total_added_m <- apply(behaveTrain[, start_diff_ind : end_diff_ind], 1, function(x) sum(ifelse(x > 0, x, 0)))
# head(as.data.frame(behaveTrain[behaveTrain$total_added_m > 0,]),20)
# table(behaveTrain$snapshot_date, behaveTrain$total_added_m)  

  # total dropped

behaveTrain$total_dropped_m <- apply(behaveTrain[start_diff_ind : end_diff_ind], 1, function(x) sum(ifelse(x < 0, x, 0)))
# head(as.data.frame(behaveTrain[behaveTrain$total_dropped_m < 0,]),20)
# table(behaveTrain$snapshot_date, behaveTrain$total_dropped_m)  

  # lag functions (2 over 2, 3 over 3 months)

# create SQL nvl() function alternative
nvl <- function (x) {
  x[is.na(x)] <- 0
  return(x)
}

head(as.data.frame(behaveTrain))
rm_DIFF_cols <- grep('_DIFF$',names(behaveTrain))
behaveTrain <- behaveTrain[,-c(rm_DIFF_cols)]
rm_pre_cols <- grep('_pre$',names(behaveTrain))
behaveTrain <- behaveTrain[,-c(rm_pre_cols)]

behaveTrain <- behaveTrain %>%
  arrange(custid, snapshot_date) %>%
  group_by(custid) %>%
  mutate(
          # average products owned (2 ad 3 month offsets)
         owned_prod_t2 = (total_owned_m + nvl(lag(total_owned_m))) / 2, 
         owned_prod_t3 = (total_owned_m + nvl(lag(total_owned_m)) + nvl(lag(total_owned_m, 2))) / 3, 
         owned_prod_p2 = (nvl(lag(total_owned_m, 2)) + nvl(lag(total_owned_m, 3))) / 2, 
         owned_prod_p3 = (nvl(lag(total_owned_m, 3)) + nvl(lag(total_owned_m, 4)) + nvl(lag(total_owned_m, 5))) / 3, 
         owned_diff_t2p2 = owned_prod_t2 - owned_prod_p2,
         owned_diff_t3p3 = owned_prod_t3 - owned_prod_p3,
          # average products added
         added_prod_t2 = (total_added_m + nvl(lag(total_added_m))) / 2, 
         added_prod_t3 = (total_added_m + nvl(lag(total_added_m)) + nvl(lag(total_added_m, 2))) / 3, 
         added_prod_p2 = (nvl(lag(total_added_m, 2)) + nvl(lag(total_added_m, 3))) / 2, 
         added_prod_p3 = (nvl(lag(total_added_m, 3)) + nvl(lag(total_added_m, 4)) + nvl(lag(total_added_m, 5))) / 3, 
         added_diff_t2p2 = added_prod_t2 - added_prod_p2,
         added_diff_t3p3 = added_prod_t3 - added_prod_p3,
          # average products dropped
         drop_prod_t2 = (total_dropped_m + nvl(lag(total_dropped_m))) / 2, 
         drop_prod_t3 = (total_dropped_m + nvl(lag(total_dropped_m)) + nvl(lag(total_dropped_m, 2))) / 3, 
         drop_prod_p2 = (nvl(lag(total_dropped_m, 2)) + nvl(lag(total_dropped_m, 3))) / 2, 
         drop_prod_p3 = (nvl(lag(total_dropped_m, 3)) + nvl(lag(total_dropped_m, 4)) + nvl(lag(total_dropped_m, 5))) / 3, 
         drop_diff_t2p2 = drop_prod_t2 - drop_prod_p2,
         drop_diff_t3p3 = drop_prod_t3 - drop_prod_p3)

behaveTrain <- behaveTrain %>%
  arrange(custid, snapshot_date) %>%
  group_by(custid) %>%
  mutate(
          # T2 - product level changes in the trailing 2 months
         ahor_owned_t2 = (ind_ahor_fin_ult1 + nvl(lag(ind_ahor_fin_ult1))) / 2,
         aval_owned_t2 = (ind_aval_fin_ult1 + nvl(lag(ind_aval_fin_ult1))) / 2,
         cco_owned_t2 = (ind_cco_fin_ult1 + nvl(lag(ind_cco_fin_ult1))) / 2,
         cder_owned_t2 = (ind_cder_fin_ult1 + nvl(lag(ind_cder_fin_ult1))) / 2,
         cno_owned_t2 = (ind_cno_fin_ult1 + nvl(lag(ind_cno_fin_ult1))) / 2,
         ctju_owned_t2 = (ind_ctju_fin_ult1 + nvl(lag(ind_ctju_fin_ult1))) / 2,
         ctma_owned_t2 = (ind_ctma_fin_ult1 + nvl(lag(ind_ctma_fin_ult1))) / 2,
         ctop_owned_t2 = (ind_ctop_fin_ult1 + nvl(lag(ind_ctop_fin_ult1))) / 2,
         ctpp_owned_t2 = (ind_ctpp_fin_ult1 + nvl(lag(ind_ctpp_fin_ult1))) / 2,
         deco_owned_t2 = (ind_deco_fin_ult1 + nvl(lag(ind_deco_fin_ult1))) / 2,
         deme_owned_t2 = (ind_deme_fin_ult1 + nvl(lag(ind_deme_fin_ult1))) / 2,
         dela_owned_t2 = (ind_dela_fin_ult1 + nvl(lag(ind_dela_fin_ult1))) / 2,
         ecue_owned_t2 = (ind_ecue_fin_ult1 + nvl(lag(ind_ecue_fin_ult1))) / 2,
         fond_owned_t2 = (ind_fond_fin_ult1 + nvl(lag(ind_fond_fin_ult1))) / 2,
         hip_owned_t2 = (ind_hip_fin_ult1 + nvl(lag(ind_hip_fin_ult1))) / 2,
         plan_owned_t2 = (ind_plan_fin_ult1 + nvl(lag(ind_plan_fin_ult1))) / 2,
         pres_owned_t2 = (ind_pres_fin_ult1 + nvl(lag(ind_pres_fin_ult1))) / 2,
         reca_owned_t2 = (ind_reca_fin_ult1 + nvl(lag(ind_reca_fin_ult1))) / 2,
         tjcr_owned_t2 = (ind_tjcr_fin_ult1 + nvl(lag(ind_tjcr_fin_ult1))) / 2,
         valo_owned_t2 = (ind_valo_fin_ult1 + nvl(lag(ind_valo_fin_ult1))) / 2,
         viv_owned_t2 = (ind_viv_fin_ult1 + nvl(lag(ind_viv_fin_ult1))) / 2,
         nomina_owned_t2 = (ind_nomina_ult1 + nvl(lag(ind_nomina_ult1))) / 2,
         nom_pens_owned_t2 = (ind_nom_pens_ult1 + nvl(lag(ind_nom_pens_ult1))) / 2,
         recibo_owned_t2 = (ind_recibo_ult1 + nvl(lag(ind_recibo_ult1))) / 2)
         
behaveTrain <- behaveTrain %>%
  arrange(custid, snapshot_date) %>%
  group_by(custid) %>%
  mutate(
# P2 - product level changes in the previous 2 months
         ahor_owned_p2 = (nvl(lag(ind_ahor_fin_ult1, 2)) + nvl(lag(ind_ahor_fin_ult1, 3))) / 2,
         aval_owned_p2 = (nvl(lag(ind_aval_fin_ult1, 2)) + nvl(lag(ind_aval_fin_ult1, 3))) / 2,
         cco_owned_p2 = (nvl(lag(ind_cco_fin_ult1, 2)) + nvl(lag(ind_cco_fin_ult1, 3))) / 2,
         cder_owned_p2 = (nvl(lag(ind_cder_fin_ult1, 2)) + nvl(lag(ind_cder_fin_ult1, 3))) / 2,
         cno_owned_p2 = (nvl(lag(ind_cno_fin_ult1, 2)) + nvl(lag(ind_cno_fin_ult1, 3))) / 2,
         ctju_owned_p2 = (nvl(lag(ind_ctju_fin_ult1, 2)) + nvl(lag(ind_ctju_fin_ult1, 3))) / 2,
         ctma_owned_p2 = (nvl(lag(ind_ctma_fin_ult1, 2)) + nvl(lag(ind_ctma_fin_ult1, 3))) / 2,
         ctop_owned_p2 = (nvl(lag(ind_ctop_fin_ult1, 2)) + nvl(lag(ind_ctop_fin_ult1, 3))) / 2,
         ctpp_owned_p2 = (nvl(lag(ind_ctpp_fin_ult1, 2)) + nvl(lag(ind_ctpp_fin_ult1, 3))) / 2,
         deco_owned_p2 = (nvl(lag(ind_deco_fin_ult1, 2)) + nvl(lag(ind_deco_fin_ult1, 3))) / 2,
         deme_owned_p2 = (nvl(lag(ind_deme_fin_ult1, 2)) + nvl(lag(ind_deme_fin_ult1, 3))) / 2,
         dela_owned_p2 = (nvl(lag(ind_dela_fin_ult1, 2)) + nvl(lag(ind_dela_fin_ult1, 3))) / 2,
         ecue_owned_p2 = (nvl(lag(ind_ecue_fin_ult1, 2)) + nvl(lag(ind_ecue_fin_ult1, 3))) / 2,
         fond_owned_p2 = (nvl(lag(ind_fond_fin_ult1, 2)) + nvl(lag(ind_fond_fin_ult1, 3))) / 2,
         hip_owned_p2 = (nvl(lag(ind_hip_fin_ult1, 2)) + nvl(lag(ind_hip_fin_ult1, 3))) / 2,
         plan_owned_p2 = (nvl(lag(ind_plan_fin_ult1, 2)) + nvl(lag(ind_plan_fin_ult1, 3))) / 2,
         pres_owned_p2 = (nvl(lag(ind_pres_fin_ult1, 2)) + nvl(lag(ind_pres_fin_ult1, 3))) / 2,
         reca_owned_p2 = (nvl(lag(ind_reca_fin_ult1, 2)) + nvl(lag(ind_reca_fin_ult1, 3))) / 2,
         tjcr_owned_p2 = (nvl(lag(ind_tjcr_fin_ult1, 2)) + nvl(lag(ind_tjcr_fin_ult1, 3))) / 2,
         valo_owned_p2 = (nvl(lag(ind_valo_fin_ult1, 2)) + nvl(lag(ind_valo_fin_ult1, 3))) / 2,
         viv_owned_p2 = (nvl(lag(ind_viv_fin_ult1, 2)) + nvl(lag(ind_viv_fin_ult1, 3))) / 2,
         nomina_owned_p2 = (nvl(lag(ind_nomina_ult1, 2)) + nvl(lag(ind_nomina_ult1, 3))) / 2,
         nom_pens_owned_p2 = (nvl(lag(ind_nom_pens_ult1, 2)) + nvl(lag(ind_nom_pens_ult1, 3))) / 2,
         recibo_owned_p2 = (nvl(lag(ind_recibo_ult1, 2)) + nvl(lag(ind_recibo_ult1, 3))) / 2)

behaveTrain <- behaveTrain %>%
  arrange(custid, snapshot_date) %>%
  group_by(custid) %>%
  mutate(         
         # T2P2 - product level t2 vs. p2 differences
         ahor_diff_t2p2 = ahor_owned_t2  - ahor_owned_p2,
         aval_diff_t2p2 = aval_owned_t2  - aval_owned_p2,
         cco_diff_t2p2 = cco_owned_t2  - cco_owned_p2,
         cder_diff_t2p2 = cder_owned_t2  - cder_owned_p2,
         cno_diff_t2p2 = cno_owned_t2  - cno_owned_p2,
         ctju_diff_t2p2 = ctju_owned_t2  - ctju_owned_p2,
         ctma_diff_t2p2 = ctma_owned_t2  - ctma_owned_p2,
         ctop_diff_t2p2 = ctop_owned_t2  - ctop_owned_p2,
         ctpp_diff_t2p2 = ctpp_owned_t2  - ctpp_owned_p2,
         deco_diff_t2p2 = deco_owned_t2  - deco_owned_p2,
         deme_diff_t2p2 = deme_owned_t2  - deme_owned_p2,
         dela_diff_t2p2 = dela_owned_t2  - dela_owned_p2,
         ecue_diff_t2p2 = ecue_owned_t2  - ecue_owned_p2,
         fond_diff_t2p2 = fond_owned_t2  - fond_owned_p2,
         hip_diff_t2p2 = hip_owned_t2  - hip_owned_p2,
         plan_diff_t2p2 = plan_owned_t2  - plan_owned_p2,
         pres_diff_t2p2 = pres_owned_t2  - pres_owned_p2,
         reca_diff_t2p2 = reca_owned_t2  - reca_owned_p2,
         tjcr_diff_t2p2 = tjcr_owned_t2  - tjcr_owned_p2,
         valo_diff_t2p2 = valo_owned_t2  - valo_owned_p2,
         viv_diff_t2p2 = viv_owned_t2  - viv_owned_p2,
         nomina_diff_t2p2 = nomina_owned_t2  - nomina_owned_p2,
         nom_pens_diff_t2p2 = nom_pens_owned_t2  - nom_pens_owned_p2,
         recibo_diff_t2p2 = recibo_owned_t2  - recibo_owned_p2)

behaveTrain <- behaveTrain %>%
  arrange(custid, snapshot_date) %>%
  group_by(custid) %>%
  mutate(
    # T3 - product level changes in the trailing 3 months
    ahor_owned_t3 = (ind_ahor_fin_ult1 + nvl(lag(ind_ahor_fin_ult1)) + nvl(lag(ind_ahor_fin_ult1,2))) / 3,
    aval_owned_t3 = (ind_aval_fin_ult1 + nvl(lag(ind_aval_fin_ult1)) + nvl(lag(ind_aval_fin_ult1,2))) / 3,
    cco_owned_t3 = (ind_cco_fin_ult1 + nvl(lag(ind_cco_fin_ult1)) + nvl(lag(ind_cco_fin_ult1,2))) / 3,
    cder_owned_t3 = (ind_cder_fin_ult1 + nvl(lag(ind_cder_fin_ult1)) + nvl(lag(ind_cder_fin_ult1,2))) / 3,
    cno_owned_t3 = (ind_cno_fin_ult1 + nvl(lag(ind_cno_fin_ult1)) + nvl(lag(ind_cno_fin_ult1,2))) / 3,
    ctju_owned_t3 = (ind_ctju_fin_ult1 + nvl(lag(ind_ctju_fin_ult1)) + nvl(lag(ind_ctju_fin_ult1,2))) / 3,
    ctma_owned_t3 = (ind_ctma_fin_ult1 + nvl(lag(ind_ctma_fin_ult1)) + nvl(lag(ind_ctma_fin_ult1,2))) / 3,
    ctop_owned_t3 = (ind_ctop_fin_ult1 + nvl(lag(ind_ctop_fin_ult1)) + nvl(lag(ind_ctop_fin_ult1,2))) / 3,
    ctpp_owned_t3 = (ind_ctpp_fin_ult1 + nvl(lag(ind_ctpp_fin_ult1)) + nvl(lag(ind_ctpp_fin_ult1,2))) / 3,
    deco_owned_t3 = (ind_deco_fin_ult1 + nvl(lag(ind_deco_fin_ult1)) + nvl(lag(ind_deco_fin_ult1,2))) / 3,
    deme_owned_t3 = (ind_deme_fin_ult1 + nvl(lag(ind_deme_fin_ult1)) + nvl(lag(ind_deme_fin_ult1,2))) / 3,
    dela_owned_t3 = (ind_dela_fin_ult1 + nvl(lag(ind_dela_fin_ult1)) + nvl(lag(ind_dela_fin_ult1,2))) / 3,
    ecue_owned_t3 = (ind_ecue_fin_ult1 + nvl(lag(ind_ecue_fin_ult1)) + nvl(lag(ind_ecue_fin_ult1,2))) / 3,
    fond_owned_t3 = (ind_fond_fin_ult1 + nvl(lag(ind_fond_fin_ult1)) + nvl(lag(ind_fond_fin_ult1,2))) / 3,
    hip_owned_t3 = (ind_hip_fin_ult1 + nvl(lag(ind_hip_fin_ult1)) + nvl(lag(ind_hip_fin_ult1,2))) / 3,
    plan_owned_t3 = (ind_plan_fin_ult1 + nvl(lag(ind_plan_fin_ult1)) + nvl(lag(ind_plan_fin_ult1,2))) / 3,
    pres_owned_t3 = (ind_pres_fin_ult1 + nvl(lag(ind_pres_fin_ult1)) + nvl(lag(ind_pres_fin_ult1,2))) / 3,
    reca_owned_t3 = (ind_reca_fin_ult1 + nvl(lag(ind_reca_fin_ult1)) + nvl(lag(ind_reca_fin_ult1,2))) / 3,
    tjcr_owned_t3 = (ind_tjcr_fin_ult1 + nvl(lag(ind_tjcr_fin_ult1)) + nvl(lag(ind_tjcr_fin_ult1,2))) / 3,
    valo_owned_t3 = (ind_valo_fin_ult1 + nvl(lag(ind_valo_fin_ult1)) + nvl(lag(ind_valo_fin_ult1,2))) / 3,
    viv_owned_t3 = (ind_viv_fin_ult1 + nvl(lag(ind_viv_fin_ult1)) + nvl(lag(ind_viv_fin_ult1,2))) / 3,
    nomina_owned_t3 = (ind_nomina_ult1 + nvl(lag(ind_nomina_ult1)) + nvl(lag(ind_nomina_ult1,2))) / 3,
    nom_pens_owned_t3 = (ind_nom_pens_ult1 + nvl(lag(ind_nom_pens_ult1)) + nvl(lag(ind_nom_pens_ult1,2))) / 3,
    recibo_owned_t3 = (ind_recibo_ult1 + nvl(lag(ind_recibo_ult1)) + nvl(lag(ind_recibo_ult1,2))) / 3)

behaveTrain <- behaveTrain %>%
  arrange(custid, snapshot_date) %>%
  group_by(custid) %>%
  mutate(
    # P3 - product level changes in the previous 3 months
    ahor_owned_p3 = mean(nvl(lag(ind_ahor_fin_ult1, 3)) + nvl(lag(ind_ahor_fin_ult1, 4)) + nvl(lag(ind_ahor_fin_ult1, 5))) / 3,
    aval_owned_p3 = mean(nvl(lag(ind_aval_fin_ult1, 3)) + nvl(lag(ind_aval_fin_ult1, 4)) + nvl(lag(ind_aval_fin_ult1, 5))) / 3,
    cco_owned_p3 = mean(nvl(lag(ind_cco_fin_ult1, 3)) + nvl(lag(ind_cco_fin_ult1, 4)) + nvl(lag(ind_cco_fin_ult1, 5))) / 3,
    cder_owned_p3 = mean(nvl(lag(ind_cder_fin_ult1, 3)) + nvl(lag(ind_cder_fin_ult1, 4)) + nvl(lag(ind_cder_fin_ult1, 5))) / 3,
    cno_owned_p3 = mean(nvl(lag(ind_cno_fin_ult1, 3)) + nvl(lag(ind_cno_fin_ult1, 4)) + nvl(lag(ind_cno_fin_ult1, 5))) / 3,
    ctju_owned_p3 = mean(nvl(lag(ind_ctju_fin_ult1, 3)) + nvl(lag(ind_ctju_fin_ult1, 4)) + nvl(lag(ind_ctju_fin_ult1, 5))) / 3,
    ctma_owned_p3 = mean(nvl(lag(ind_ctma_fin_ult1, 3)) + nvl(lag(ind_ctma_fin_ult1, 4)) + nvl(lag(ind_ctma_fin_ult1, 5))) / 3,
    ctop_owned_p3 = mean(nvl(lag(ind_ctop_fin_ult1, 3)) + nvl(lag(ind_ctop_fin_ult1, 4)) + nvl(lag(ind_ctop_fin_ult1, 5))) / 3,
    ctpp_owned_p3 = mean(nvl(lag(ind_ctpp_fin_ult1, 3)) + nvl(lag(ind_ctpp_fin_ult1, 4)) + nvl(lag(ind_ctpp_fin_ult1, 5))) / 3,
    deco_owned_p3 = mean(nvl(lag(ind_deco_fin_ult1, 3)) + nvl(lag(ind_deco_fin_ult1, 4)) + nvl(lag(ind_deco_fin_ult1, 5))) / 3,
    deme_owned_p3 = mean(nvl(lag(ind_deme_fin_ult1, 3)) + nvl(lag(ind_deme_fin_ult1, 4)) + nvl(lag(ind_deme_fin_ult1, 5))) / 3,
    dela_owned_p3 = mean(nvl(lag(ind_dela_fin_ult1, 3)) + nvl(lag(ind_dela_fin_ult1, 4)) + nvl(lag(ind_dela_fin_ult1, 5))) / 3,
    ecue_owned_p3 = mean(nvl(lag(ind_ecue_fin_ult1, 3)) + nvl(lag(ind_ecue_fin_ult1, 4)) + nvl(lag(ind_ecue_fin_ult1, 5))) / 3,
    fond_owned_p3 = mean(nvl(lag(ind_fond_fin_ult1, 3)) + nvl(lag(ind_fond_fin_ult1, 4)) + nvl(lag(ind_fond_fin_ult1, 5))) / 3,
    hip_owned_p3 = mean(nvl(lag(ind_hip_fin_ult1, 3)) + nvl(lag(ind_hip_fin_ult1, 4)) + nvl(lag(ind_hip_fin_ult1, 5))) / 3,
    plan_owned_p3 = mean(nvl(lag(ind_plan_fin_ult1, 3)) + nvl(lag(ind_plan_fin_ult1, 4)) + nvl(lag(ind_plan_fin_ult1, 5))) / 3,
    pres_owned_p3 = mean(nvl(lag(ind_pres_fin_ult1, 3)) + nvl(lag(ind_pres_fin_ult1, 4)) + nvl(lag(ind_pres_fin_ult1, 5))) / 3,
    reca_owned_p3 = mean(nvl(lag(ind_reca_fin_ult1, 3)) + nvl(lag(ind_reca_fin_ult1, 4)) + nvl(lag(ind_reca_fin_ult1, 5))) / 3,
    tjcr_owned_p3 = mean(nvl(lag(ind_tjcr_fin_ult1, 3)) + nvl(lag(ind_tjcr_fin_ult1, 4)) + nvl(lag(ind_tjcr_fin_ult1, 5))) / 3,
    valo_owned_p3 = mean(nvl(lag(ind_valo_fin_ult1, 3)) + nvl(lag(ind_valo_fin_ult1, 4)) + nvl(lag(ind_valo_fin_ult1, 5))) / 3,
    viv_owned_p3 = mean(nvl(lag(ind_viv_fin_ult1, 3)) + nvl(lag(ind_viv_fin_ult1, 4)) + nvl(lag(ind_viv_fin_ult1, 5))) / 3,
    nomina_owned_p3 = mean(nvl(lag(ind_nomina_ult1, 3)) + nvl(lag(ind_nomina_ult1, 4)) + nvl(lag(ind_nomina_ult1, 5))) / 3,
    nom_pens_owned_p3 = mean(nvl(lag(ind_nom_pens_ult1, 3)) + nvl(lag(ind_nom_pens_ult1, 4)) + nvl(lag(ind_nom_pens_ult1, 5))) / 3,
    recibo_owned_p3 = mean(nvl(lag(ind_recibo_ult1, 3)) + nvl(lag(ind_recibo_ult1, 4)) + nvl(lag(ind_recibo_ult1, 5))) / 3)

behaveTrain <- behaveTrain %>%
  arrange(custid, snapshot_date) %>%
  group_by(custid) %>%
  mutate(
         # T3P3 - product level t3 vs. p3 differences
         ahor_diff_t3p3 = ahor_owned_t3  - ahor_owned_p3 ,
         aval_diff_t3p3 = aval_owned_t3  - aval_owned_p3 ,
         cco_diff_t3p3 = cco_owned_t3  - cco_owned_p3 ,
         cder_diff_t3p3 = cder_owned_t3  - cder_owned_p3 ,
         cno_diff_t3p3 = cno_owned_t3  - cno_owned_p3 ,
          ctju_diff_t3p3 = ctju_owned_t3  - ctju_owned_p3 ,
          ctma_diff_t3p3 = ctma_owned_t3  - ctma_owned_p3 ,
          ctop_diff_t3p3 = ctop_owned_t3  - ctop_owned_p3 ,
          ctpp_diff_t3p3 = ctpp_owned_t3  - ctpp_owned_p3 ,
          deco_diff_t3p3 = deco_owned_t3  - deco_owned_p3 ,
          deme_diff_t3p3 = deme_owned_t3  - deme_owned_p3 ,
          dela_diff_t3p3 = dela_owned_t3  - dela_owned_p3 ,
          ecue_diff_t3p3 = ecue_owned_t3  - ecue_owned_p3 ,
          fond_diff_t3p3 = fond_owned_t3  - fond_owned_p3 ,
          hip_diff_t3p3 = hip_owned_t3  - hip_owned_p3 ,
          plan_diff_t3p3 = plan_owned_t3  - plan_owned_p3 ,
          pres_diff_t3p3 = pres_owned_t3  - pres_owned_p3 ,
          reca_diff_t3p3 = reca_owned_t3  - reca_owned_p3 ,
          tjcr_diff_t3p3 = tjcr_owned_t3  - tjcr_owned_p3 ,
          valo_diff_t3p3 = valo_owned_t3  - valo_owned_p3 ,
          viv_diff_t3p3 = viv_owned_t3  - viv_owned_p3 ,
          nomina_diff_t3p3 = nomina_owned_t3  - nomina_owned_p3 ,
          nom_pens_diff_t3p3 = nom_pens_owned_t3  - nom_pens_owned_p3 ,
          recibo_diff_t3p3 = recibo_owned_t3  - recibo_owned_p3
          )

head(as.data.frame(behaveTrain))
rm_p2p3_cols <- grep('_p2$|_p3$',names(behaveTrain))
behaveTrain <- behaveTrain[,-c(rm_p2p3_cols)]

rm(end_diff_ind, end_ind, i, new_index, rm_DIFF_cols, rm_pre_cols, start_diff_ind, start_ind, rm_p2p3_cols)

##########################

# Save objects to not run everthing above again
# http://stackoverflow.com/questions/19967478/how-to-save-data-file-into-rdata

# saveRDS(behaveTrain, "behaveTrain.rds")
# saveRDS(target_vars, "target_vars.rds")

setwd('Y:/R/sant')
list.files()

library(data.table)
library(tidyverse)
library(lubridate)
library(dplyr)
library(pryr)
library(caret)
library(doParallel)
library(foreach)

behaveTrain <- readRDS("behaveTrain.rds")
target_vars <- readRDS("target_vars.rds")

##########################

head(target_vars)
target_var_rem <- apply(target_vars[,grep('ind_ahor_fin_ult1',names(target_vars)) : grep('ind_recibo_ult1',names(target_vars))], 2, sum)

target_var_rem <- which(target_var_rem < 500)
rem_t <- as.vector(names(target_var_rem))

rare_target_vars <- target_vars[,c('snapshot_date','prediction_date','custid',rem_t)]

target_vars[,rem_t] <- NULL

rm(rem_t, target_var_rem, features, features_t, i, new_names, rm_DIFF_cols, rm_p2p3_cols, rm_pre_cols, x, end_ind, new_index, start_ind)

##########################
#### Model building
##########################

TrainTestCutoff04 <- ymd('2016-04-01')
TrainTestCutoff05 <- ymd('2016-05-01')
StartDateCutoff07 <- ymd('2015-07-01')

# create initial modelling dataset
ModelTrain <- behaveTrain[behaveTrain$snapshot_date > StartDateCutoff07 & behaveTrain$snapshot_date < TrainTestCutoff04,]
ModelTrain <- left_join(ModelTrain, target_vars, by = c('snapshot_date','custid'))

# Delete columns not needed for the model
ModelTrain$tenure_days.y <- NULL
ModelTrain$prediction_date <- NULL
ModelTrain <- ModelTrain[, grep('tenure_days.x',names(ModelTrain)) : ncol(ModelTrain)]

rm(behaveTrain, target_vars, tempTarget1, rare_target_vars, ModelFit_cco_1)

mem_used()
gc(reset = TRUE) 

train_control <- trainControl(
  method = "repeatedcv"
  , number = 5
  , repeats = 3
  , allowParallel = TRUE
   , classProbs = TRUE
   , summaryFunction = twoClassSummary
)

loop_start <- grep('ind_cco_fin_ult1.y',names(ModelTrain))
loop_end <- grep('ind_recibo_ult1.y',names(ModelTrain))
behave_end_limit <- grep('recibo_diff_t3p3', names(ModelTrain))

mtryVar <- ceiling(sqrt(behave_end_limit))
lowerMtry <- floor(mtryVar / 2)
upperMtryLow <- floor(mtryVar * 2)
upperMtryMid <- floor(mtryVar * 3)
upperMtryHigh <- floor(mtryVar * 5)
newGrid = expand.grid(mtry = c(lowerMtry, mtryVar, upperMtryLow, upperMtryMid, upperMtryHigh))

for (i in loop_start : loop_end) {

  # Undersample the data to have a balanced dataset
  ModelTrainPos <- ModelTrain[ModelTrain[, i] == 1, ]
  ModelTrainZero <- ModelTrain[ModelTrain[, i] == 0, ]
  
  sampSize <- nrow(ModelTrainPos) * 4
  ModelTrainZero <- ModelTrainZero[sample(1 : nrow(ModelTrainZero), sampSize), ]
  
  train_data <- rbind(ModelTrainPos, ModelTrainZero)
  train_data <- train_data[!is.na(train_data[, i]), ]
  rm(ModelTrainPos, ModelTrainZero)
  
  target_var <- make.names(as.factor(train_data[[i]]))
  x_vals <- train_data[, 1 : behave_end_limit]
  
  # enable parallel start a parallel cluster
  cl <- makePSOCKcluster((detectCores()-1))
  clusterEvalQ(cl, library(foreach))
  registerDoParallel(cl)
  
  system.time(
    ModelFit <- train(x = x_vals, y = target_var
                    , method = 'parRF'
                    , trControl = train_control
                    , metric = 'Spec' # try different ones like 'ROC' / 'Accuracy' / 'Spec'
                    , preProcess = c('scale','center','BoxCox')
                    , ntree = 200
                    , tuneGrid = newGrid
                    )
  )

  stopCluster(cl) # stop a parallel cluster
  
  newname <- paste0('ModelFit_',names(ModelTrain)[i])
  assign(newname, ModelFit)
  rm(ModelFit, train_data, target_var, x_vals)
  
  mem_used()
  gc(reset = TRUE) 

}
# END OF LOOP

# SAVE MODELS INTO FILES (put into loop)
saveRDS(model, file = "model.rds")
model <- readRDS("model.rds")

# score and evaluate the training dataset -- add everything below to a FOR loop
predVals1 <- predict(ModelFit_cco_1, ModelTrain)
confusionMatrix(predVals1, ModelTrain$ind_cco_fin_ult1.y)

# score and evaluate the test dataset (exclude NAs first) -- add everything below to a FOR loop
ModelTest <- behaveTrain[behaveTrain$snapshot_date > TrainTestCutoff04,]
ModelTest <- left_join(ModelTest, target_vars, by = c('snapshot_date','custid'))

ModelTest_noNA <- ModelTest[!is.na(ModelTest$ind_cco_fin_ult1.y),]
predTest1 <- predict(ModelFit_cco_1, ModelTest_noNA)
confusionMatrix(predTest1, ModelTest_noNA$ind_cco_fin_ult1.y)

# remove variables
rm(predTest1, ModelTest_noNA, predVals1)

# formula = Survived ~ Pclass + Sex + Age + SibSp + Parch + Fare + Embarked
# train_control = trainControl(method="cv", number=5)
# modelLog = train(formula, data=train, method="glm", family=binomial, trControl=train_control)
# PredTrain = predict(modelLog, newdata=train, type="raw")
# table(train$Survived, PredTrain > 0.5)
# 
# # scoring and measuring model fit
# probsTest <- predict(tune, test, type = "prob")
# threshold <- 0.5
# pred      <- factor( ifelse(probsTest[, "yes"] > threshold, "yes", "no") )
# pred      <- relevel(pred, "yes")   # you may or may not need this; I did
# confusionMatrix(pred, test$response)

##########################
#### Old explorations
##########################

# # explore tenure
# tenure_dist <- as.numeric(difftime(ymd(Sys.Date()),train$fecha_alta))
# qplot(tenure_dist)
#
# tenure_dist <- as.data.frame(tenure_dist)
# tenure_dist <- tenure_dist %>% group_by(tenure_dist) %>% summarise(quantity = n()) %>% arrange(-quantity)
# head(tenure_dist)

# check memory status
mem_used()
ls()
sapply(ls(), function(x) object_size(get(x))) %>% sort %>% tail(5)
mem_change(rm(train,train_temp))
rm(target_vars)
gc()
mem_used()
gc(reset = TRUE) 

# example:
# mydata <- data.frame(id = rep(LETTERS[1:5],2), gr = rep(letters[c(6,7)],10), dt = 2:11)
# mydata %>% arrange(id, gr) %>% group_by(id) %>% mutate(cumsum = cumsum(dt))
# mydata %>% arrange(id, gr) %>% group_by(id) %>% mutate(diff = dt - lag(dt), diff2 = dt - lag(dt,2))

# Test out removing NearZeroVar variables + highly correlated variables - LATER

# ModelVARS_NZV <- ModelVARS[,-nearZeroVar(ModelVARS)]
# head(ModelVARS_NZV)
# 
# modelFit_NOZERO <- train(ind_cco_fin_ult1.y ~.,
#                          data = ModelVARS_NZV,
#                          method = 'glm',
#                          family = binomial,
#                          trControl = train_control,
#                          preProcess = c('scale','center','BoxCox')
# )
# 
# predVals2 <- predict(modelFit_NOZERO, ModelTrain_NoZero)
# confusionMatrix(predVals2, ModelTrain_NoZero$ind_cco_fin_ult1.y)

#stop parallel
# stopCluster(cl)
```